//
//  GalleryTableViewController.swift
//  ImageGallery
//
//  Created by Kirill Kirikov on 6/11/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class GalleryTableViewController: UITableViewController {
    
    let model = ImagesModel()
    var imagesTask:URLSessionTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagesTask = model.loadImages(with: "swift") { (images, error) in
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.imageURLs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
        
        let url = model.imageURLs[indexPath.row]
        cell.task?.cancel()
        cell.galleryImage.image = nil
        cell.task = model.loadImage(url: url) { (image) in
            cell.galleryImage.image = image
        }
        
        return cell
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let cell = sender as? ImageTableViewCell else {
            return
        }
        
        guard let destination = segue.destination as? PreviewViewController else {
            return
        }
        
        destination.image = cell.galleryImage?.image
        
    }


}
