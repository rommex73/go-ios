//
//  ImagesModel.swift
//  ImageGallery
//
//  Created by Kirill Kirikov on 6/11/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

protocol ImagesModelProtocol {
    var imageURLs:[URL] { get }
    func loadImages(with query:String, callback: @escaping ((_ images:[URL]?, _ error:Error?)->Void)) -> URLSessionTask
    
    func loadImage(url:URL, callback:@escaping (_ image:UIImage)->Void) -> URLSessionDownloadTask
}

class ImagesModel: ImagesModelProtocol {
    
    private(set) var imageURLs:[URL] = []
    
    private var urlSession = URLSession(configuration: .default)
    
    func loadImage(url:URL, callback:@escaping (_ image:UIImage)->Void) -> URLSessionDownloadTask {
        
        let task = urlSession.downloadTask(with: url) { (location, response, error) in
            if location != nil {
                guard let data = try? Data(contentsOf: location!, options: []) else {
                    return
                }
                
                guard let image = UIImage(data: data) else {
                    return
                }
                
                DispatchQueue.main.async {
                    callback(image)
                }
            }
        }
        
        task.resume()
        return task
    }
    
    func loadImages(with query:String, callback: @escaping ((_ images:[URL]?, _ error:Error?)->Void)) -> URLSessionTask {
        
        var components = URLComponents(string: "https://api.flickr.com/services/rest/")!
        components.queryItems = [
            URLQueryItem(name: "method", value: "flickr.photos.search"),
            URLQueryItem(name: "api_key", value: "52e152e165ce50bb554446f59c89ea02"),
            URLQueryItem(name: "tags", value: query),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "nojsoncallback", value: "1"),
        ]
        let task = urlSession.dataTask(with: components.url!) {
            (data:Data?, response:URLResponse?, error:Error?) in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    callback(nil, error)
                }
                
                guard let data = data else {
                    return callback(nil, nil)
                }
                
                do {
                    var result:[URL] = []
                    
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    let responseDic = json as? [String: Any]
                    let photos = responseDic?["photos"] as? [String: Any]
                    guard let photo = photos?["photo"] as? [[String:Any]] else {
                        return callback(nil, nil)
                    }
                    
                    for photoItem in photo {
                        
                        let farmId = String(photoItem["farm"] as! Int )
                        let serverId = photoItem["server"] as! String
                        let id = photoItem["id"] as! String
                        let secret = photoItem["secret"] as! String
                        
                        let url = URL(string: "https://farm\(farmId).staticflickr.com/\(serverId)/\(id)_\(secret).jpg")!
                        
                        result.append(url)
                    }
                    
                    
                    print("Result: \(result)")
                    
                    self.imageURLs = result
                    
                    callback(result, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
        task.resume()
        return task
    }
    
}
