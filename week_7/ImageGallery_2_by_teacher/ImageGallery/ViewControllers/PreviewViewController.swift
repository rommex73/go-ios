//
//  PreviewViewController.swift
//  ImageGallery
//
//  Created by Kirill Kirikov on 6/11/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {

    var image:UIImage? {
        didSet {
            if isViewLoaded {
                imageView.image = image
            }
        }
    }
    
    @IBOutlet weak var imageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
