//: Playground - noun: a place where people can play

import UIKit

/*
enum MyOptional <T> {
    case some(T)
    case none
}

let myOptionalString: MyOptional<String> = MyOptional.some("MyString")
*/

let x0:String? = nil

// is 

let x1 = Optional<String>.none

// or

let x2:String? = "hello"

// is

let x3 = Optional<String>.some("hello")

var y = x3!

// is

switch x3 {
case .some (let unwrapped): y = unwrapped
case .none: //rise an exception!
    break
}