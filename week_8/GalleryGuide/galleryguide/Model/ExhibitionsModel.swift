//
//  ExhibitionsModel.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation

class ExhibitionsModel {
    
    static var instance: ExhibitionsModel = ExhibitionsModel()
    
    private(set) var exhibitions:[ExhibitionVO] = []
    
    func loadExhibitions( callback: @escaping () -> (), method: String ) {
        DataLoader().downLoadExhibitionsAndGalleries( callback: callback, downloadingMethod: method )
        
        
        //loadExhibitions()
    }
    
    func setExhibitions (_ ex:[ExhibitionVO] ) {
        
        exhibitions = ex
    }
    
    
    func loadFilteredData (tag: Int, callback: @escaping () -> () ) {
        DataLoader().downLoadExhibitionsAndGalleriesByIndex ( callback: callback, downloadingMethod: tag )
    }
    
}
