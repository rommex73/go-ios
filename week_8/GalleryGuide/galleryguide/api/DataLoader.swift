//
//  DataLoader.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 5/31/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation
import Alamofire

extension Date {
    static func from(string:String?) -> Date? {
        
        guard let string = string else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss'Z'"
        return dateFormatter.date(from: string)
    }
}

extension Date {
    static func stringFrom(date:Date?) -> String? {
        
        guard let date = date else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
}

protocol Parsable {
    init?(jsonDictionary: [String: Any])
}

extension ExhibitionVO: Parsable {
    
    init?(jsonDictionary: [String: Any])  {
        guard let name = jsonDictionary["name"] as? String else {
            return nil
        }
        
        self.name = name
        self.id = jsonDictionary["_id"] as? String
        self.about = jsonDictionary["about"] as? String
        self.authorName = jsonDictionary["authorName"] as? String
        self.authorDescription = jsonDictionary["authorDescription"] as? String
        self.startDate = Date.from(string: jsonDictionary["dateStart"] as? String)
        self.endDate = Date.from(string: jsonDictionary["dateEnd"] as? String)
        self.gallery = nil
    }
}

extension GalleryVO: Parsable {
    init?(jsonDictionary: [String: Any])  {
        guard let name = jsonDictionary["name"] as? String else {
            return nil
        }
        
        //        guard let id = jsonDictionary["objectId"] as? String else {
        //            return nil
        //        }
        
        self.id = jsonDictionary["objectId"] as? String
        self.name = name
        self.galleryDescription = jsonDictionary["galleryDescription"] as? String
        self.email = jsonDictionary["email"] as? String
        self.facebook = jsonDictionary["facebook"] as? String
        self.city = jsonDictionary["city"] as? String
    }
}

class DataLoader {
    
    let allExhibithionsUrl = "https://gallery-guru-prod.herokuapp.com/exhibitions?skip=0&limit=100"
    
    let urlPostfix = "?skip=0&limit=100"
    
    let exhibitionsURLOptions = [
        "all": "https://gallery-guru-prod.herokuapp.com/exhibitions",
        "opening": "https://gallery-guru-prod.herokuapp.com/exhibitions/opening",
        "lastchance": "https://gallery-guru-prod.herokuapp.com/exhibitions/lastchance",
        "popular": "https://gallery-guru-prod.herokuapp.com/exhibitions/popular",
        "near": "https://gallery-guru-prod.herokuapp.com/exhibitions/near"
    ]
    
    let exhibitionsURLOptionsArray = [
        "https://gallery-guru-prod.herokuapp.com/exhibitions/near?lat=32.1631844&lon=34.9289502",
        "https://gallery-guru-prod.herokuapp.com/exhibitions/popular" ,
        "https://gallery-guru-prod.herokuapp.com/exhibitions/opening",
        "https://gallery-guru-prod.herokuapp.com/exhibitions/lastchance",
        "https://gallery-guru-prod.herokuapp.com/exhibitions"
    ]
    
    
    func loadJSONArray(with filename: String) -> [[String: Any]]? {
        
        
        guard let url = Bundle.main.url(forResource: filename, withExtension: "json") else {
            return nil
        }
        
        guard let rawData = try? Data(contentsOf: url) else {
            return nil
        }
        
        guard let rawArray = try? JSONSerialization.jsonObject(with: rawData) as? [[String: Any]] else {
            return nil
        }
        
        return rawArray
        
    }
    
    func pushExhibitions () {
        ExhibitionsModel.instance.setExhibitions(exhibitions)
    }
    
    var exhibitions:[ExhibitionVO] = []
    
    func downLoadExhibitionsAndGalleriesByIndex (callback: @escaping () -> (), downloadingMethod: Int ) {
        var galleries:[String: GalleryVO] = [:]
        
        Alamofire.request( exhibitionsURLOptionsArray[downloadingMethod] + urlPostfix ).responseData { response in
            
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let data = response.result.value {
                // print("DATA: \(data)") // serialized json response
                
                let rawArray = try? JSONSerialization.jsonObject(with: data) as? [[String: Any]]    //data as? [[String:Any]]
                
                if let array = rawArray {
                    if array != nil {
                    for item in array! {
                        
                        if let parsedExhibition = ExhibitionVO(jsonDictionary: item) {
                            
                            var mGallery : GalleryVO?
                            
                            if let rawGallery = item["gallery"] as? [String:Any] {  // there is item
                                if let parsedItem = GalleryVO(jsonDictionary: rawGallery) {
                                    mGallery = parsedItem
                                    if let id = parsedItem.id {
                                        galleries[id] = parsedItem }
                                }
                                
                            }
                            
                            var ex = parsedExhibition
                            ex.gallery = mGallery
                            self.exhibitions.append(ex)
                        }
                        
                        
                        
                    }
                    
                    }
                }
                
                
                self.pushExhibitions()
                callback()
                
            }
        }
    }
    
    
    
    
    
    func downLoadExhibitionsAndGalleries (callback: @escaping () -> (), downloadingMethod: String ) {
        var galleries:[String: GalleryVO] = [:]
        
        Alamofire.request( exhibitionsURLOptions[downloadingMethod]! + urlPostfix ).responseData { response in
            
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let data = response.result.value {
                // print("DATA: \(data)") // serialized json response
                
                let rawArray = try? JSONSerialization.jsonObject(with: data) as? [[String: Any]]    //data as? [[String:Any]]
                
                if let array = rawArray {
                    for item in array! {
                        
                        if let parsedExhibition = ExhibitionVO(jsonDictionary: item) {
                            
                            var mGallery : GalleryVO?
                            
                            if let rawGallery = item["gallery"] as? [String:Any] {  // there is item
                                if let parsedItem = GalleryVO(jsonDictionary: rawGallery) {
                                    mGallery = parsedItem
                                    if let id = parsedItem.id {
                                        galleries[id] = parsedItem }
                                }
                                
                            }
                            
                            var ex = parsedExhibition
                            ex.gallery = mGallery
                            self.exhibitions.append(ex)
                        }
                        
                        
                        
                    }
                    
                    
                }
                
                
                self.pushExhibitions()
                callback()
                
            }
        }
    }
    
    func loadExhibitions() -> [ExhibitionVO]  {
        
        var galleries:[String: GalleryVO] = [:]
        var exhibitions:[ExhibitionVO] = []
        
        //downLoadExhibitionsAndGalleries (callback: <#() -> ()#>)
        
        load(filename: "galleries") { (item: GalleryVO, json) in
            if let id = item.id {
                galleries[id] = item
            }
        }
        
        load(filename: "exhibitions") { (item: ExhibitionVO, json) in
            let galleryPointer = json["_p_gallery"] as? String
            let galleryId = galleryPointer?.components(separatedBy: "$").last
            
            var exhibition = item
            if galleryId != nil {
                exhibition.gallery = galleries[galleryId!]
            }
            
            exhibitions.append(exhibition)
        }
        
        return exhibitions
    }
    
    private func load<T: Parsable>(filename: String, iterationHandler:((_ item: T,_ rawJSON:[String: Any])->())) {
        if let array = loadJSONArray(with: filename) {
            for item in array {
                if let parsedItem = T(jsonDictionary: item) {
                    iterationHandler(parsedItem, item)
                }
            }
        }
    }
}
