//
//  ExhibitionsListViewController.swift
//  galleryguide
//
//  Created by Admin on 6/26/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit
import SDWebImage

class ExhibitionsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterBack: UIView!
    @IBOutlet weak var dataFilterButton: UIButton!
    
    @IBAction func showDataFilterMenu(_ sender: Any) {
        filterBack.isHidden = false
        dataFilterButton.isHidden = true
    }
    
    
    @IBAction func filterDataButtonPressed(_ sender: Any) {
        
        if let button = sender as? UIButton {
            let tag = button.tag
            ExhibitionsModel.instance.loadFilteredData (tag: tag) { [unowned self] in
                
                DispatchQueue.main.async {
                self.exhibitions = ExhibitionsModel.instance.exhibitions
                self.tableView.reloadData()  
                }
                
            }
            dataFilterButton.titleLabel?.text = filterOptionsText[tag]
        }
        filterBack.isHidden = true
        dataFilterButton.isHidden = false
        
        
    }
    
    let filterOptionsText = [ "Near me",
                          "Most Popular",
                          "Opening",
                          "Last Chance",
                          "All"]
    
    
    
    var exhibitions:[ExhibitionVO]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        filterBack.isHidden = true
        dataFilterButton.isHidden = false
        dataFilterButton.titleLabel?.text = filterOptionsText[4]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exhibitions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DesignCell") as! DesignTableViewCell
        let exhibition = exhibitions[indexPath.row]
        cell.exhibitionName?.text = exhibition.name
        cell.authorName?.text = exhibition.authorName
        cell.galleryName?.text = exhibition.gallery?.name
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let s = segue.identifier
        if segue.identifier == "showDetails2" {
            let cell = sender as! DesignTableViewCell
            let indexPath = tableView.indexPath(for: cell)!
            let exhibition = exhibitions[indexPath.row]
            
            let destination = segue.destination as! DetailsViewController
            destination.exhibition = exhibition
        }
    }
    
    
    
    func loadNewData () {
        ExhibitionsModel.instance.loadExhibitions(callback: didLoadNewData, method: "all" )
        self.exhibitions = ExhibitionsModel.instance.exhibitions
    }
    
    func didLoadNewData () {
        tableView.reloadData()
    }
}
