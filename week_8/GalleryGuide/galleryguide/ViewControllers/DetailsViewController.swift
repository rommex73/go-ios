//
//  DetailsViewController.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    

    @IBOutlet weak var exhibitionName: UILabel!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var startandEndDates: UILabel!
    
    var exhibition:ExhibitionVO!
    
    class instack {
        var i = [Int]()
        func add (x:Int) {
            i.append(x)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exhibitionName.text = exhibition.name
        authorName.text = exhibition.authorName
        
        if let start = exhibition.startDate, let end = exhibition.endDate  {
           startandEndDates.text = Date.stringFrom (date: start)! + " - " + Date.stringFrom (date: end)!
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)

    }
}
