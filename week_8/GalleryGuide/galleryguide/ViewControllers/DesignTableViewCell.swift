//
//  DesignTableViewCell.swift
//  galleryguide
//
//  Created by Admin on 6/26/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class DesignTableViewCell: UITableViewCell {

    @IBOutlet weak var exhibitionName: UILabel!
    @IBOutlet weak var galleryName: UILabel!
    @IBOutlet weak var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
