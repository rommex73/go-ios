//
//  LoadingViewController.swift
//  galleryguide
//
//  Created by Admin on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        ExhibitionsModel.instance.loadExhibitions()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let exhibitionsVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionsViewController")
        
        //self.navigationController?.pushViewController(exhibitionsVC, animated: true)
        
        self.navigationController?.setViewControllers([exhibitionsVC], animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
