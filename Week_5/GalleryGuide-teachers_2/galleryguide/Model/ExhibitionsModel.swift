//
//  ExhibitionsModel.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation



protocol AbstractVO {
    
}


//protocol SelfDownloader {
//    func loadData ()
//}

//class Test: Array<Int> {
//    
//}

struct DataArray<T>{
    
    var array: Array<T>
    
    subscript (index:Int) -> T {
        get {
        return array[index]
        }
        
        set (newValue) {
            array[index] = newValue
        }
    }
    
    mutating func append (element:T) {
        array.append(element)
    }
    
    var count: Int {
        return array.count
    }
    
}



extension DataArray where T == ExhibitionVO {
    
     func loadData() -> DataArray<ExhibitionVO> {
        
        let dataLoader = DataLoader()
        
        //let galleries = dataLoader.loadGalleries()
        var result = DataArray<ExhibitionVO> (array:[])
        
        let mArray = dataLoader.loadBundleFileIntoArray("exhibitions")
        
        for aDictionary in mArray {
            
            var galleryID:String = aDictionary["_p_gallery"] as! String
            galleryID = galleryID.components(separatedBy: "$").last!
            
            
            if let exhibition = ExhibitionVO.buildFrom (dictionary: aDictionary, galleryID:galleryID)
            { result.append (element: exhibition) }
            
        }
        return result
    }
    
}

extension DataArray where T == WorkVO {
    
    mutating func loadData() {
        
    }
    
}




struct DataDictionary<T> {
    var dictionary: [String:T]
    
    subscript (index:String) -> T? {
        get {
            return dictionary[index]
        }
        
        set (newValue) {
            dictionary[index] = newValue
        }
    }
    
//    var count: Int {
//        return dictionary.count
//    }
    
    
//    func loadData() {
//        
//    }
}


extension DataDictionary where T == GalleryVO {
    
    mutating func loadData() {
        
        let dataLoader = DataLoader()
        let array = dataLoader.loadBundleFileIntoArray("galleries")
        
        for aDictionary in array {
            
            if let gallery = GalleryVO.buildFrom (dictionary: aDictionary)
            { self[gallery.id] = gallery  }
        }

    }
    
}


class ExhibitionsModel {
    
    static var instance: ExhibitionsModel = ExhibitionsModel()
    
    private(set) var exhibitions = DataArray<ExhibitionVO> (array: [])
    private(set) var works = DataArray<WorkVO> (array:[])
    private(set) var galleries = DataDictionary<GalleryVO> (dictionary: [:])
    
    func loadExhibitions() {
        exhibitions = exhibitions.loadData()
    }
}
