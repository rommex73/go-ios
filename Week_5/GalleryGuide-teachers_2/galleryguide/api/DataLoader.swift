//
//  DataLoader.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 5/31/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation

extension Date {
    static func from(string:String?) -> Date? {
        
        guard let string = string else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss'Z'"
        return dateFormatter.date(from: string)
    }
}

class DataLoader {
    
    
    
    ////////////--------------------4-------Generics
    
    
    func loadExhibitions_4 () {
        
        ExhibitionsModel.instance.exhibitions.loadData()
        
    }
    
    func loadGalleries_4 () {
        
     //   ExhibitionsModel.instance.galleries.loadData()
        
    }
    
    
    
    
    func loadBundleFileIntoArray (_ filename: String) -> [[String: Any]] {
        
        guard let url = Bundle.main.url(forResource: filename, withExtension: "json") else {
            return []
        }
        
        guard let rawData = try? Data(contentsOf: url) else {
            return []
        }
        
        guard let rawArray = try? JSONSerialization.jsonObject(with: rawData) as? [[String: Any]] else {
            return []
        }
        
        return rawArray!
    }
    

    
    ////////////--------------------3
    
    
    
    func loadExhibitions_3 () -> [ExhibitionVO]  {
        let galleries = loadGalleries()
        var result:[ExhibitionVO] = []
        
        let array = loadBundleFileIntoArray("exhibitions")
        
        for aDictionary in array {
            
            var galleryID:String = aDictionary["_p_gallery"] as! String
            galleryID = galleryID.components(separatedBy: "$").last!
            
            
            if let exhibition = ExhibitionVO.buildFrom (dictionary: aDictionary, galleryID:galleryID)
            { result.append(exhibition) }
            
        }
        
        return result
    }
    
    
    
    private func loadGalleries_3() -> [String: GalleryVO] {
        
        var result:[String: GalleryVO] = [:]
        let array = loadBundleFileIntoArray("galleries")
        
        for aDictionary in array {
        
            if let gallery = GalleryVO.buildFrom (dictionary: aDictionary)
            { result[gallery.id] = gallery  }
            
        }
        
        return result
    }
    
    
    
    ///////////-----------------------2
    
    func loadExhibitions_2 () -> [ExhibitionVO]  {
        let galleries = loadGalleries()
        var result:[ExhibitionVO] = []
        
        let array = loadBundleFileIntoArray("exhibitions")
        for exhibitionDictionary in array {
            
            var galleryID:String = exhibitionDictionary["_p_gallery"] as! String
            galleryID = galleryID.components(separatedBy: "$").last!
            
            if let gallery = galleries[galleryID] {
                
                let exhibition = ExhibitionVO(
                    id: exhibitionDictionary["_id"] as? String,
                    name: exhibitionDictionary["name"] as! String,
                    about: exhibitionDictionary["about"] as? String,
                    authorName: exhibitionDictionary["authorName"] as? String,
                    authorDescription: exhibitionDictionary["authorDescription"] as? String,
                    startDate: Date.from(string: exhibitionDictionary["dateStart"] as? String),
                    endDate: Date.from(string: exhibitionDictionary["dateEnd"] as? String),
                    gallery: gallery)
                
                result.append(exhibition)
            }
        }
        return result
    }
    
    private func loadGalleries_2() -> [String: GalleryVO] {
        
        var result:[String: GalleryVO] = [:]
        let array = loadBundleFileIntoArray("galleries")
        
        for galleryDictionary in array {
            
            let gallery = GalleryVO(
                id:         galleryDictionary["_id"] as! String,
                name:       galleryDictionary["name"] as! String,
                galleryDescription: galleryDictionary["galleryDescription"] as? String,
                email:      galleryDictionary["email"] as? String,
                facebook:   galleryDictionary["facebook"] as? String,
                city:       galleryDictionary["city"] as? String,
                works:      [WorkVO]())
            
            result[gallery.id] = gallery
        }
        
        return result
    }
    
    
    

    
    
    
    
    
    
    ////
    
    
    func loadExhibitions() -> [ExhibitionVO]  {
        
        let galleries = loadGalleries()
        
        var result:[ExhibitionVO] = []
        
        guard let url = Bundle.main.url(forResource: "exhibitions", withExtension: "json") else {
            return result
        }
        
        guard let exhibitionsRawData = try? Data(contentsOf: url) else {
            return result
        }
        
        guard let exhibitionsRawArray = try? JSONSerialization.jsonObject(with: exhibitionsRawData) as? [[String: Any]] else {
            return result
        }
        
        if let array = exhibitionsRawArray {
            for exhibitionDictionary in array {
                
                var galleryID:String = exhibitionDictionary["_p_gallery"] as! String
                galleryID = galleryID.components(separatedBy: "$").last!
                
                if let gallery = galleries[galleryID] {
                    
                    let exhibition = ExhibitionVO(
                        id: exhibitionDictionary["_id"] as? String,
                        name: exhibitionDictionary["name"] as! String,
                        about: exhibitionDictionary["about"] as? String,
                        authorName: exhibitionDictionary["authorName"] as? String,
                        authorDescription: exhibitionDictionary["authorDescription"] as? String,
                        startDate: Date.from(string: exhibitionDictionary["dateStart"] as? String),
                        endDate: Date.from(string: exhibitionDictionary["dateEnd"] as? String),
                        gallery: gallery)
                    
                    result.append(exhibition)
                }
            }
        }
        return result
    }
    
    
    
    func loadGalleries() -> [String: GalleryVO] {
        
        var result:[String: GalleryVO] = [:]
        
        guard let url = Bundle.main.url(forResource: "galleries", withExtension: "json") else {
            return result
        }
        
        guard let galleriesRawData = try? Data(contentsOf: url) else {
            return result
        }
        
        guard let galleriesRawArray = try? JSONSerialization.jsonObject(with: galleriesRawData) as? [[String: Any]] else {
            return result
        }
        
        if let array = galleriesRawArray {
            for galleryDictionary in array {
                
                let gallery = GalleryVO(
                    id:         galleryDictionary["_id"] as! String,
                    name:       galleryDictionary["name"] as! String,
                    galleryDescription: galleryDictionary["galleryDescription"] as? String,
                    email:      galleryDictionary["email"] as? String,
                    facebook:   galleryDictionary["facebook"] as? String,
                    city:       galleryDictionary["city"] as? String,
                    works:      [WorkVO]())
                
                result[gallery.id] = gallery
            }
        }
        
        return result
    }
    
    private func loadWorks() -> [String:WorkVO] {
        return [:]
    }
    
    
}



protocol Parsable {
    
}



////////////////by Kirill
fileprivate extension ExhibitionVO:Parsable {
    init? (dictionary: [String: Any]) {
        
    }
}

private func load <T:Parsable> (filename:String) -> [T} {
    
}]


// let galleries:[GalleryVO] = load (filename: "galleries")
