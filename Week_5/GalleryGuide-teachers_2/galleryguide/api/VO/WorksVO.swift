//
//  WorksVO.swift
//  galleryguide
//
//  Created by Admin on 6/5/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation

struct WorkVO {
    var id:String?
    var name:String
    var about:String?
    var authorName:String?
    var authorDescription:String?
    var startDate:Date?
    var endDate:Date?
    var gallery:GalleryVO
}
