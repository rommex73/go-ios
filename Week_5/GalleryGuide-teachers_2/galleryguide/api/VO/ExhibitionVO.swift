//
//  ExhibitionVO.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 5/31/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation

struct ExhibitionVO: AbstractVO { // : DataCategory
    var id:String?
    var name:String
    var about:String?
    var authorName:String?
    var authorDescription:String?
    var startDate:Date?
    var endDate:Date?
    var gallery:GalleryVO
    
    static func buildFrom (dictionary: [String:Any], galleryID:String) -> ExhibitionVO? {
        
        if let gallery = ExhibitionsModel.instance.galleries[galleryID] {
            let exhibition = ExhibitionVO (
                id: dictionary["_id"] as? String,
                name: dictionary["name"] as! String,
                about: dictionary["about"] as? String,
                authorName: dictionary["authorName"] as? String,
                authorDescription: dictionary["authorDescription"] as? String,
                startDate: Date.from(string: dictionary["dateStart"] as? String),
                endDate: Date.from(string: dictionary["dateEnd"] as? String),
                gallery: gallery
            )
            
            return exhibition
        } else {   return nil   }
    }
    
    /*
     var filename:String  { return "exhibitions" }
     
     func buildElement (from dictionary:  [String:Any]) -> DataCategory? {
     
     var galleryID:String = dictionary["_p_gallery"] as! String
     galleryID = galleryID.components(separatedBy: "$").last!
     
     if let gallery = ExhibitionsModel.instance.galleries[galleryID] {
     
     let exhibition = ExhibitionVO (
     id: dictionary["_id"] as? String,
     name: dictionary["name"] as! String,
     about: dictionary["about"] as? String,
     authorName: dictionary["authorName"] as? String,
     authorDescription: dictionary["authorDescription"] as? String,
     startDate: Date.from(string: dictionary["dateStart"] as? String),
     endDate: Date.from(string: dictionary["dateEnd"] as? String),
     gallery: gallery)
     
     return exhibition
     }
     return nil
     }
     */
}
