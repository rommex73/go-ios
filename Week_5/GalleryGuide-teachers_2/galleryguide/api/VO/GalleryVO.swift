//
//  GalleryVO.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 5/31/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import Foundation

struct GalleryVO: AbstractVO  {
    
    var id:String!
    var name:String!
    var galleryDescription:String?
    var email:String?
    var facebook:String?
    var city:String?
    var works:[WorkVO]
    
    static func buildFrom (dictionary: [String:Any]) -> GalleryVO? {
        
        
            let gallery = GalleryVO(
                id:         dictionary["_id"] as! String,
                name:       dictionary["name"] as! String,
                galleryDescription: dictionary["galleryDescription"] as? String,
                email:      dictionary["email"] as? String,
                facebook:   dictionary["facebook"] as? String,
                city:       dictionary["city"] as? String,
                works:      [WorkVO]())
            
            return gallery
        
    }
    
}
