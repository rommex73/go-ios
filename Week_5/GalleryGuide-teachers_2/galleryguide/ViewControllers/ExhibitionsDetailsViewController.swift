//
//  ExhibitionsDetailsViewController.swift
//  galleryguide
//
//  Created by Admin on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class ExhibitionsDetailsViewController: UIViewController {
    
    public var exhibitionData: ExhibitionVO?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        name.text = exhibitionData?.name
        about.text = exhibitionData?.about
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var about: UILabel!
}
