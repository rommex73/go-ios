//
//  ExhibitionsViewController.swift
//  GalleryGuide
//
//  Created by Kirill Kirikov on 6/4/17.
//  Copyright © 2017 Seductive. All rights reserved.
//

import UIKit

class ExhibitionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ExhibitionsModel.instance.exhibitions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ExhibitionsTableViewCell
        let exhibition = ExhibitionsModel.instance.exhibitions[indexPath.row]
        cell.name.text = exhibition.name
        cell.authorName.text = exhibition.authorName
        cell.galleryName.text = exhibition.gallery.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //ExhibitionsModel.nameDetails = ExhibitionsModel.instance.exhibitions[indexPath.row].name
        //ExhibitionsModel.aboutDetails = ExhibitionsModel.instance.exhibitions[indexPath.row].about
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let exhibitionsDetailsVC = storyboard.instantiateViewController(withIdentifier: "ExhibitionDetailsViewController") as! ExhibitionsDetailsViewController
        
        exhibitionsDetailsVC.exhibitionData = ExhibitionsModel.instance.exhibitions[indexPath.row]
        
        self.navigationController?.pushViewController(exhibitionsDetailsVC, animated: true)
        
    }
    
}
