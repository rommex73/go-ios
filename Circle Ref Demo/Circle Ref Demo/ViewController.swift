//
//  ViewController.swift
//  Circle Ref Demo
//
//  Created by Admin on 6/21/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let queue = DispatchQueue (label: "--", qos: .utility, attributes: [], autoreleaseFrequency: .inherit, target: nil)
        
        let mainQueue = DispatchQueue.global()
        
        queue.async {
            print ("Async task completed")
        }
        
        print ("Main queue carries on")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

