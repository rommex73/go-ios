//
//  ViewController.swift
//  Calculator_Start
//
//  Created by Kirill Kirikov on 4/22/17.
//  Copyright © 2017 GoIT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var inputLabel: UILabel!

    var model = CalculatorModel()
    var inputStarted = false
    var dotPressed = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func input(_ sender: UIButton) {
        
        if (!inputStarted) {
            inputLabel.text = ""
        }
        
        inputStarted = true
        
        let pressedDigit = sender.title(for: .normal)
        
        // exclude adding ZERO to a ZERO
        if !( inputLabel.text == "0" && pressedDigit == "0" )  {
            
            // if zero was entered, it should not become leading
            if inputLabel.text == "0" {
                inputLabel.text = ""
            }
            inputLabel.text = "\(inputLabel.text!)\(pressedDigit!)" }
    }
    
    @IBAction func putDot(_ sender: Any) {
//        if (!inputStarted) {
//            inputLabel.text = "0"
//        }
        
        if dotPressed { return }
        dotPressed = true
        inputStarted = true
        inputLabel.text = "\(inputLabel.text!)."
    }
    
    @IBAction func evaluate(_ sender: UIButton) {
        
        let result = model.evaluate(operand: inputToDigit())
        putToDisplay (number: result)
        inputStarted = false
        dotPressed = false
    }
    
    @IBAction func operation(_ sender: UIButton) {
        
        if let operation = sender.title(for: .normal) {
            let result = model.operation(operand: inputToDigit(), operation: operation)
            putToDisplay (number: result)
            inputStarted = false
            dotPressed = false
        }
    }
    
    @IBAction func reset(_ sender: UIButton) {
        model = CalculatorModel()
        inputStarted = false
        dotPressed = false
        dotPressed = false
        inputLabel.text = "0"
        
    }
    
    
    // display as Int or as Double
    func putToDisplay ( number : Double ) {
        
        if number.isNaN || number.isInfinite {
            inputLabel.text = "ERROR"
            return
        }
        
        let intNumber = Int (number)
        if Double (intNumber) == number {
            inputLabel.text = "\(intNumber)"
        } else {
            inputLabel.text = "\(number)"
        }
    }
    
    
    func inputToDigit() -> Double? {
        
        if !inputStarted {
            return nil
        }
        
        guard let input = inputLabel.text else {
            return 0
        }
        
        return Double(input) ?? 0
    }
    
}

