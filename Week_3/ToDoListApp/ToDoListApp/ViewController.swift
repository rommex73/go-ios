//
//  ViewController.swift
//  ToDoListApp
//
//  Created by Admin on 5/14/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit


private enum ToDoItemPriority {
    case normal
    case low
    case high
    
    func color() -> UIColor {
        
        switch self {
        case .high: return .red
        case .low: return .blue
        case .normal: return UIColor (red: 5/255, green: 225/255, blue: 119/255, alpha: 1)
        }
        
        //return UIColor.black
    }
}

private protocol ToDoItem {
    var title:String {
        get
    }
    var priority:ToDoItemPriority {
        get
    }
    
    var icon: UIImage {
        get
    }
}

private class ClassworkToDoItem: ToDoItem, NSCoding {
    var title: String
    var priority: ToDoItemPriority
    var icon: UIImage {
        get {
            return     UIImage (named: "icon_2")!
        }
    }
    
    init (title:String, priority:ToDoItemPriority) {
        self.title = title
        self.priority = priority
    }
    
    required init? (coder aDecoder:NSCoder){
        title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        priority = aDecoder.decodeObject(forKey: "priority") as? ToDoItemPriority ?? .high
    }
    
    func encode (with aCoder: NSCoder) {
        aCoder.encode (title, forKey: "title")
    }
}


private struct HomeworkToDoItem: ToDoItem {
    var title: String
    var priority: ToDoItemPriority
    var icon: UIImage {
        get {
            return     UIImage (named: "icon_1")!
        }
    }
    

}

private struct GameworkToDoItem: ToDoItem {
    var title: String
    var priority: ToDoItemPriority
    var icon: UIImage {
        get {
            return     UIImage (named: "icon_0")!
        }
    }
}


private class TodoItemsDataSource: NSObject, UITableViewDataSource {
    
    fileprivate var items: [ToDoItem] = [
        ClassworkToDoItem (title:"Tell about UITableView", priority: .normal),
        ClassworkToDoItem (title:"Tell about Data Sources", priority: .high),
        ClassworkToDoItem (title:"Check homework", priority: .low),
        ClassworkToDoItem (title:"Tell about Polymorphism", priority: .normal),
        GameworkToDoItem (title:"Play Quake", priority: .high),
        HomeworkToDoItem (title:"Tell about Methods", priority: .normal)
    
    ]
    
//        "To do homework",
//        "Go to gym",
//        "Call mom",
//        "Read about VC",
//        "Drink Wine",
//        "Go Sleep"
//        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
//        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
//        "20", "21", "22", "23", "24", "25", "26", "27", "28", "29"
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print ("numberOfRowsInSection: \(section)")
        return items.count
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        print ("cellForRowAt: \(indexPath.row)")
        
        let reuseIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)! as! ToDoItemTableViewCell
        //cell.textLabel?.text = items[indexPath.row].title
        
        cell.titleLabel.text = items[indexPath.row].title
        cell.priorityView.backgroundColor = items[indexPath.row].priority.color()
        cell.iconImageView.image = items[indexPath.row].icon
        
        return cell
        
    }
    
    func addItem (item:ToDoItem) {
        //items.append(item)
        
        var myItems = items
        myItems.append(item)
        
        let defaults ....
    }
    
}


private class ToDoItemsFSDataSource: TodoItemsDataSource {
    
    override var items: [ToDoItem] {
        get {
            let defaults = UserDefaults.standard
        
            return defaults.array(forKey: "ToDoItems" ) as! [ToDoItem] ?? []
        }
        set {
            
        }
    }
    
}

class ViewController: UIViewController {
    
    private let dataSource = TodoItemsDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let defaults = UserDefaults.standard
        defaults.setValue(1, forKey: "ToDoItems")
    
        
        tableView.dataSource = dataSource
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addNewClassworkItem(_ sender: UIButton) {
        
        let item = ClassworkToDoItem (title: "New Item", priority: .high)
    }
    
    @IBOutlet weak var tableView: UITableView!

    
    
    


}

