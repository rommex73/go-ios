//
//  ToDoItemTableViewCell.swift
//  ToDoListApp
//
//  Created by Admin on 5/14/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ToDoItemTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var priorityView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print ("awakeFromNib")
        
        // creation of the view cell object
        
        priorityView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
