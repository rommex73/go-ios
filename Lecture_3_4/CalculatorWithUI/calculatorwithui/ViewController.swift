//
//  ViewController.swift
//  CalculatorWithUI
//
//  Created by Kirill Kirikov on 5/7/17.
//  Copyright © 2017 GoIT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var didInputStarted:Bool = false
    
    // "." implementation
    var dotEntered = false
    
    var model = CalculatorModel()
    var currentDisplayValue: Double {
        set {
            
            // display integers without dot
            let intNumber = Int (newValue)
            if Double (intNumber) == newValue {
                inputDisplay.text = "\(intNumber)"
            } else {
                inputDisplay.text = "\(newValue)"
            }
            
        }
        get {
            return Double(inputDisplay.text!)!
        }
    }
    
    @IBOutlet weak var inputDisplay: UITextField!
    
    @IBAction func touchDigit(_ sender: UIButton) {
        print("Digit: \(sender.currentTitle!)")
        
        if didInputStarted {
            if !(inputDisplay.text == "0" && sender.currentTitle == "0") {
                
                if inputDisplay.text == "0" {
                    inputDisplay.text = sender.currentTitle!
                }
                else
                {
                    inputDisplay.text = inputDisplay.text! + sender.currentTitle!
                }
                
            }
        } else
        {
            inputDisplay.text = sender.currentTitle!
            didInputStarted = true }
    }
    
    @IBAction func dotPressed(_ sender: UIButton) {
        if dotEntered || !didInputStarted { return }
        dotEntered = true
        didInputStarted = true
        inputDisplay.text = "\(inputDisplay.text!)."
    }
    
    
    
    @IBAction func performOperation(_ sender: UIButton) {
        
        guard let currentValue = Double(inputDisplay.text!) else {
            return
        }
        
        model.setOperand(currentValue)
        model.performOperation(sender.currentTitle!)
        currentDisplayValue = model.result ?? 0
        
        didInputStarted = false
        dotEntered = false
    }
}

