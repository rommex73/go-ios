//
//  DreamItemViewController.swift
//  CoreDataExample
//
//  Created by Admin on 7/2/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Photos

class DreamItemViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var sliderPrice: UISlider!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didSaveButtonPressed(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let dreamItem = DreamItem(context: context)
        
        dreamItem.createdat = NSDate()
        dreamItem.titlr =  titleTextField.text
        dreamItem.picture = imageView.image
        dreamItem.price = Double(sliderPrice.value)
        appDelegate.saveContext()
        
    }

    @IBAction func didTouchSelectImage(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        present (imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let selectedImage = info [UIImagePickerControllerEditedImage] as? UIImage {
            imageView.image = selectedImage
        }
    }

}
