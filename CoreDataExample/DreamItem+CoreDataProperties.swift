//
//  DreamItem+CoreDataProperties.swift
//  CoreDataExample
//
//  Created by Admin on 7/2/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData


extension DreamItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DreamItem> {
        return NSFetchRequest<DreamItem>(entityName: "DreamItem")
    }

    @NSManaged public var titlr: String?
    @NSManaged public var price: Double
    @NSManaged public var picture: NSObject?
    @NSManaged public var createdat: NSDate?

}
