//
//  ToDoItemTableViewCell.swift
//  ToDoListApp
//
//  Created by Kirill Kirikov on 5/14/17.
//  Copyright © 2017 GoIT. All rights reserved.
//

import UIKit

protocol ToDoItemTableViewCellDelegate {
    func titleDidChange(in cell:ToDoItemTableViewCell)
    func togglePriority (in cell:ToDoItemTableViewCell)
}

class ToDoItemTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var priorityButton: UIButton!
    //@IBOutlet weak var priorityView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var labelTextField: UITextField!
    
    var delegate:ToDoItemTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()        
        priorityButton.layer.cornerRadius = 5
        print("awakeFromNib")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        delegate?.titleDidChange(in: self)
    }

    @IBAction func priorityButtonWasPressed(_ sender: Any) {
        delegate?.togglePriority(in: self)
    }
}
