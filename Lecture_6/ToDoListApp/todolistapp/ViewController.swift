//
//  ViewController.swift
//  ToDoListApp
//
//  Created by Kirill Kirikov on 5/14/17.
//  Copyright © 2017 GoIT. All rights reserved.
//

import UIKit

private enum ToDoItemPriority:Int {
    case normal
    case low
    case high
    
    func color() -> UIColor {
        switch self {
        case .high:
            return .red
        case .low:
            return .blue
        case .normal:
            return UIColor(red: 5/255, green: 225/255, blue: 119/255, alpha: 1)
        }
    }
}

private protocol ToDoItem {
    var title:String {
        get
        set
    }
    
    var priority:ToDoItemPriority {
        get
        set
    }
    
    var icon:UIImage {
        get
    }
}

private class BaseToDoItem: NSObject, ToDoItem, NSCoding {
    var title: String
    var priority: ToDoItemPriority
    
    var icon: UIImage {
        get {
            return #imageLiteral(resourceName: "icon_0")
        }
    }
    
    init(title: String, priority: ToDoItemPriority) {
        self.title = title
        self.priority = priority
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        let intPriority = aDecoder.decodeInteger(forKey: "priority")  ?? 0
        self.priority = ToDoItemPriority(rawValue: intPriority)!
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(priority.rawValue, forKey: "priority")
    }
    
    
    
}



private class ClassworkToDoItem: BaseToDoItem {

    
    override var icon: UIImage {
        get {
            return #imageLiteral(resourceName: "icon_2")
        }
    }
    
}





private class HomeworkToDoItem: BaseToDoItem {

    override var icon: UIImage {
        get {
            return #imageLiteral(resourceName: "icon_0")
        }
    
    }
    
}






private class GameToDoItem: BaseToDoItem {

    override var icon: UIImage {
        get {
            return #imageLiteral(resourceName: "icon_1")
        }
    }
}








class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    //private var dataSource:TodoItemsDataSource?
    
    fileprivate var items:[ToDoItem] = [
        ClassworkToDoItem(title: "Tell about Polymorphysm", priority: .high),
        HomeworkToDoItem(title: "Do my homework", priority: .normal),
        GameToDoItem(title: "Play Quake 3", priority: .normal),
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
        tableView.dataSource = self
        tableView.delegate = self
        
        //dataSource?.tableView = tableView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    

    @IBAction func addItemButtonPressed(_ sender: UIBarButtonItem) {
        print ("plus pressed")
        
        let alertController = UIAlertController(title: "Create an Item", message: "Choose the kind:", preferredStyle: .actionSheet)
        
        // Create the action.
        let classworkAction = UIAlertAction(title: "Classwork", style: .default) { (action) in
            self.addNewClassworkItem()
        }
        let homeworkAction = UIAlertAction(title: "Homework", style: .default) { (action) in
            self.addNewHomeworkItem()        }
        let gameAction = UIAlertAction(title: "Game", style: .default) { (action) in
            self.addNewGameItem()
        }
//        let classworkAction = UIAlertAct (ion(title: "Classwork", style: .default) { action in
//            self.addNewClassworkItem() )
//        }
        
        // Add the action.
        alertController.addAction(classworkAction)
        alertController.addAction(homeworkAction)
        alertController.addAction(gameAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func addNewClassworkItem() {
        let item = ClassworkToDoItem(title: "New Item", priority: .low)
        addItem(item: item)
        tableView.reloadData()
    }
    
    func addNewHomeworkItem() {
        let item = HomeworkToDoItem(title: "New Item", priority: .low)
        addItem(item: item)
        tableView.reloadData()
    }
    
    func addNewGameItem() {
        let item = GameToDoItem(title: "New Item", priority: .low)
        addItem(item: item)
        tableView.reloadData()
    }
}



extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("NumberOfRowsInSection: \(section)")
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("CellForRowAt: \(indexPath.row)")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ToDoItemTableViewCell
        
        cell.labelTextField.text = items[indexPath.row].title
        //cell.titleLabel.text = items[indexPath.row].title
        cell.priorityButton.backgroundColor = items[indexPath.row].priority.color()
        cell.iconImageView.image = items[indexPath.row].icon
        cell.delegate = self
        
        cell.delegate = self
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            
            //data
            deleteItem(from: indexPath.row)
            
            //table
            //tableView.reloadData()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.right)
        }
    }
    
    fileprivate func addItem(item: ToDoItem) {
        items.append(item)
        save()
    }
    
    func deleteItem (from row: Int) {
        items.remove(at: row)
        save()
    }
    
    func save () {
        let result = NSKeyedArchiver.archiveRootObject(items, toFile: "/Users/admin/Desktop/ToDoList.bin")
    }
    
    func load() {
        if let obj = NSKeyedUnarchiver.unarchiveObject(withFile: "/Users/admin/Desktop/ToDoList.bin") as? [ToDoItem] {
            items = obj
        }
        
    }
}

extension ViewController: ToDoItemTableViewCellDelegate {
    func titleDidChange(in cell: ToDoItemTableViewCell) {
        if let index = tableView?.indexPath(for: cell) {
            items [index.row].title = cell.labelTextField.text ?? ""
            save()
        }
    }
    
    func togglePriority (in cell:ToDoItemTableViewCell) {
        guard let index = tableView?.indexPath(for: cell) else {
            return
        }
        let priority = items [index.row].priority
        let enumIndex = ( priority.rawValue + 1 ) % 3
        items [index.row].priority = ToDoItemPriority(rawValue: enumIndex)!
        tableView?.reloadData()
        save()
    }
}

extension ViewController:  UITableViewDelegate {
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

